import 'package:quizzler/question.dart';

class QuizzBrain {
  int _index = 0;
  List<Question> _questions = [
    Question(
      'You can lead a cow down stairs but not up stairs.',
      false,
    ),
    Question(
      'Approximately one quarter of human bones are in the feet.',
      true,
    ),
    Question(
      'A slug\'s blood is green.',
      true,
    ),
  ];
  void nextQuestion() {
    if (_index < _questions.length - 1) _index++;
  }

  void resetIndex() {
    _index = 0;
  }

  bool isFinished() {
    if (_index == _questions.length - 1)
      return true;
    else
      return false;
  }

  String getQuestionText() {
    return _questions[_index].questionText;
  }

  bool getQuestionAnswer() {
    return _questions[_index].questionAnswer;
  }
}
