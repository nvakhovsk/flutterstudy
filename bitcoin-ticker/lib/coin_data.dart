import 'dart:convert';

import 'package:bitcoin_ticker/secret.dart';
import 'package:http/http.dart' as http;

const List<String> currenciesList = [
  'AUD',
  'BRL',
  'CAD',
  'CNY',
  'EUR',
  'GBP',
  'HKD',
  'IDR',
  'ILS',
  'INR',
  'JPY',
  'MXN',
  'NOK',
  'NZD',
  'PLN',
  'RON',
  'RUB',
  'SEK',
  'SGD',
  'USD',
  'ZAR'
];

const List<String> cryptoList = [
  'BTC',
  'ETH',
  'LTC',
];

class CoinData {
  CoinData();
  Future getCoinData({String from, String to}) async {
    http.Response response = await http.get(
        'https://rest.coinapi.io/v1/exchangerate/${from}/${to}?apikey=${apiToken}');
    if (response.statusCode == 200) {
      String data = response.body;
      return jsonDecode(data);
    } else {
      print('status code' + response.statusCode.toString());
    }
  }
}
