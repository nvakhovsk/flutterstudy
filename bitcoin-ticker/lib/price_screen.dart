import 'dart:io' show Platform;
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'coin_data.dart';

class PriceScreen extends StatefulWidget {
  @override
  _PriceScreenState createState() => _PriceScreenState();
}

class _PriceScreenState extends State<PriceScreen> {
  String selectedCurrency = 'AUD';
  List<String> currencyRate = new List<String>(cryptoList.length);

  List<Padding> CreateRow() {
    List<Padding> res = [];
    for (int i = 0; i < cryptoList.length; i++) {
      res.add(Padding(
        padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 28.0),
        child: Text(
          '${i + 1} ${cryptoList[i]} = ${currencyRate[i]} ${selectedCurrency}',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 20.0,
            color: Colors.white,
          ),
        ),
      ));
    }
    return res;
  }

  DropdownButton AndroidDropDown() {
    List<DropdownMenuItem> res = [];
    for (String currency in currenciesList) {
      res.add(
        DropdownMenuItem(
          child: Text(currency),
          value: currency,
        ),
      );
    }

    return DropdownButton(
      value: selectedCurrency,
      items: res,
      onChanged: (value) {
        setState(() {
          selectedCurrency = value;
          getCurrencyValue();
        });
      },
    );
  }

  CupertinoPicker IOSPicker() {
    List<Text> res = [];
    for (String currency in currenciesList) {
      res.add(Text(currency));
    }

    return CupertinoPicker(
      backgroundColor: Colors.lightBlue,
      itemExtent: 32.0,
      onSelectedItemChanged: (value) {
        setState(() {
          selectedCurrency = currenciesList[value];
          getCurrencyValue();
        });
      },
      children: res,
    );
  }

  void getCurrencyValue() async {
    for (int i = 0; i < cryptoList.length; i++) {
      var response = await CoinData()
          .getCoinData(from: cryptoList[i], to: selectedCurrency);
      var rate = response['rate'].toStringAsFixed(2);
      currencyRate[i] = rate;
    }
  }

  @override
  void initState() {
    getCurrencyValue();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('🤑 Coin Ticker'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(18.0, 18.0, 18.0, 0),
            child: Card(
              color: Colors.lightBlueAccent,
              elevation: 5.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Column(
                children: CreateRow(),
              ),
            ),
          ),
          Container(
            height: 150.0,
            alignment: Alignment.center,
            padding: EdgeInsets.only(bottom: 30.0),
            color: Colors.lightBlue,
            child: Platform.isIOS ? IOSPicker() : AndroidDropDown(),
          ),
        ],
      ),
    );
  }
}
