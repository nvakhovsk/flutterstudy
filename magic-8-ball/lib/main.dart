import 'package:flutter/material.dart';
import 'dart:math';

void main() => runApp(
      MaterialApp(
        home: Scaffold(
          backgroundColor: Colors.yellow[900],
          appBar: AppBar(
            title: Text('Ask your question'),
            backgroundColor: Colors.blueGrey[900],
          ),
          body: MagicB(),
        ),
      ),
    );

class MagicB extends StatefulWidget {
  @override
  _MagicBState createState() => _MagicBState();
}

class _MagicBState extends State<MagicB> {
  int ballNum = Random().nextInt(5) + 1;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        children: [
          Expanded(
            child: FlatButton(
              onPressed: () {
                setState(() {
                  ballNum = Random().nextInt(5) + 1;
                });

                print(ballNum);
              },
              child: Image.asset('images/ball$ballNum.png'),
            ),
          ),
        ],
      ),
    );
  }
}
