import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.amber[200],
        appBar: AppBar(
          title: Text('I am Poor'),
          backgroundColor: Colors.amber[800],
        ),
        body: Center(
          child: Image(
            image: AssetImage('images/illust57-5502.jpg'),
          ),
        ),
      ),
    ),
  );
}
